class Potepan::CategoriesController < ApplicationController
  def show
    @taxonmies = Spree::Taxonomy.includes(:root)
    @taxon = Spree::Taxon.find(params[:id])
    @product_boxs = @taxon.all_products.includes(master: [:default_price, :images])
  end
end
