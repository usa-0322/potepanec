require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  given!(:taxonomy) { create(:taxonomy, name: "Categories") }
  given!(:outer) { taxonomy.root.children.create(name: "Outer") }
  given!(:bottoms) { taxonomy.root.children.create(name: "Bottoms") }
  given!(:parker) { create(:product, name: "Parker", price: "20.99", taxons: [outer]) }
  given!(:cardigan) { create(:product, name: "Cardigan", price: "10.99", taxons: [outer]) }
  given!(:jeans) { create(:product, name: "Jeans", price: "30.99", taxons: [bottoms]) }

  background do
    visit potepan_category_path(taxonomy.taxons.first.id)
  end

  scenario "商品カテゴリから商品詳細へ移動する" do
    # 商品情報
    expect(title).to have_content "Categories | Potepanec"
    expect(page).to have_content "Parker"
    expect(page).to have_content "20.99"
    expect(page).to have_content "Cardigan"
    expect(page).to have_content "10.99"
    expect(page).to have_content "Jeans"
    expect(page).to have_content "30.99"
    # 詳細画面移動
    click_on "Parker"
    expect(current_path).to eq potepan_product_path(parker.id)
    expect(title).to have_content "Parker | Potepanec"
    expect(page).to have_link "一覧ページへ戻る"
  end

  scenario "パネルリンクからカテゴリーページへ移動する" do
    # パネルリンク
    expect(page).to have_link "Outer(2)"
    expect(page).to have_link "Bottoms(1)"
    # カテゴリページ切り替え
    click_on "Outer(2)"
    expect(current_path).to eq potepan_category_path(outer.id)
    expect(page).to have_content "Parker"
    expect(page).to have_content "Cardigan"
    expect(page).not_to have_content "Jeans"
  end
end
