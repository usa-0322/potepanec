require 'rails_helper'

RSpec.feature "Products", type: :feature do
  given(:category) { create(:taxon, name: "Categories") }
  given(:brand) { create(:taxon, name: "Brand") }
  given(:product) { create(:product, name: "T-shirt", price: "10.00", taxons: [category, brand]) }

  background do
    visit potepan_product_path(product.id)
  end

  scenario "商品のデータが表示される" do
    expect(title).to have_content "T-shirt | Potepanec"
    expect(page).to have_content "T-shirt"
    expect(page).to have_content "10.00"
    expect(page).to have_content product.description
  end

  scenario "ホームへ移動する" do
    click_on "Home", match: :prefer_exact
    expect(current_path).to eq potepan_path
  end

  scenario "商品詳細から商品カテゴリへ移動する" do
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(category.id)
  end
end
