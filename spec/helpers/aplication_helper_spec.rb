require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "タイトル表示" do
    it "引数があるときpage_title | base_titleと表示されること" do
      expect(helper.full_title(page_title: "test")).to match "test | Potepanec"
    end

    it "引数がないときbasa_titleと表示されること" do
      expect(helper.full_title(page_title: "")).to match "Potepanec"
    end

    it "引数がnilのときbase_titleと表示されること" do
      expect(helper.full_title(page_title: nil)).to match "Potepanec"
    end
  end
end
