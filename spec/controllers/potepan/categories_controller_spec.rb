require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "カテゴリー詳細ページアクセス" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:not_product) { create(:product) }

    before do
      get :show, params: { id: taxon.id }
    end

    context "カテゴリーが存在するとき" do
      it "正しく表示されること" do
        expect(response).to have_http_status "200"
      end

      it "showページがレンダリングされること" do
        expect(response).to render_template :show
      end

      it "@taxonが適切であること" do
        expect(assigns(:taxon)).to eq taxon
      end

      it "@taxonomiesが適切であること" do
        expect(assigns(:taxonmies)).to eq [taxonomy]
      end

      it "適切な@productsがアサインされていること" do
        expect(assigns(:product_boxs)).to eq [product]
      end
    end

    context "カテゴリーが商品と紐づいていないとき" do
      it "商品が取得できないこと" do
        expect(taxon.products.first).not_to eq not_product
      end
    end
  end
end
