require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "商品詳細ページアクセス" do
    let(:product) { create(:product, name: 'T-shirts') }

    before do
      get:show, params: { id: product.id }
    end

    context "商品が存在するとき" do
      it "正しく表示されること" do
        expect(response).to have_http_status "200"
      end

      it "showページがレンダリングされること" do
        expect(response).to render_template :show
      end

      it "インスタンス変数が適切であること" do
        expect(assigns(:product)).to eq product
      end
    end
  end
end
